// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//<---  Login module   --->
Cypress.Commands.add('Login', (email, password) => {
    cy.get('input[name="username"]')
        .type(email)

    // For check the password control
    cy.get('input[name="password"]')
        .type(password, { log: false })

    // Code commented for disabled the ReCAPTCHA
    cy.get('iframe')
            .first()
            .its('0.contentDocument.body')
            .should('not.be.undefined')
            .and('not.be.empty')
            .then(cy.wrap)
            .find('#recaptcha-anchor')
            .should('be.visible')
            .click()
            .pause()

    //   Interval time for captcha and user need to fill by manual
    //     .wait(30000)


    // cy.get('iframe')
    //     .first()
    //     .its('0.contentDocument.body')
    //     .then(cy.wrap)
    //     .find('#recaptcha-anchor')
    //     .click()
    // .pause()

    // For check the login button
    cy.get('.card-content > .text-center > .btn')
        .wait(2000)
        .click()
        .wait(2000)
})
//<---   /Login module   --->

//<---   Excel conversion module   --->
Cypress.Commands.add("parseXlsx", (inputFile) => {
    return cy.task('parseXlsx', { filePath: inputFile })
});
//<---   /Excel conversion module   --->


//<---   API request   --->
Cypress.Commands.add("apiRequest", (apiurl, apiMethod, accesstoken, apiBody) => {
    cy.request({
        url: apiurl,
        method: apiMethod,
        headers: {
            authorization: accesstoken
        },
        body: apiBody,
    })
})
//<---   /API request   --->