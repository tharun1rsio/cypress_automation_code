/// <reference types="cypress" />
import 'cypress-file-upload';
const loginData = require('../fixtures/Login.json')
describe('Coontractor Creation', () => {
    it('Open the 1PS page', () => {
        cy.visit('http://localhost:3001/')
        Cypress.on('uncaught:exception', (err, runnable) => { return false; })
    })

    it('data conversion xlsx to json', () => {
        cy.Login(loginData.username, loginData.password)
        cy.get(':nth-child(1) > .dropdown-toggle')
            .click()
        cy.get('.open > .dropdown-menu > :nth-child(2) > a')
            .click()
        cy.get('a > .icon-container > .fa')
            .click()
        cy.get('#logo')
        .attachFile({ filePath: '../data/1ps-logo.png'});
        // < --- Save value-- ->
        //     cy.get(':nth-child(1) > .dropdown-toggle').invoke('text').as('value').then((value) => {
        //         cy.log(value)
        //     })
        // < --- /Save value --->

        // cy.parseXlsx('cypress/data/testdata.xlsx').then((jsonData) => {
        //     const rowLength = Cypress.$(jsonData[0].data).length
        //     for (let index = 0; index < rowLength; index++) {
        //         var jsonData = jsonData[index].data
        //         console.log(jsonData[index].data)
        //         cy.writeFile("cypress/fixtures/xlsxData.json", { username: jsonData[0][0], password: jsonData[0][1] })
        //     }
        // })
        // cy.readFile('cypress/data/testdata.xlsx').then((data) => {
        //     cy.log(JSON.stringify(data))
        //     cy.writeFile('cypress/fixtures/testdata.json', data , 'binary')
        // })
        // cy.readFile('cypress/data/testdata.xlsx')
        //     .then((data) => {
        //         cy.task('csvToJson', data).then((data) => {
        //             //process data
        //         })
        //     })
        cy.log("Not Include this output")
        cy.parseXlsx("cypress/data/testdata.xlsx").then(
            jsonData => {
                cy.log(jsonData)
                cy.log(jsonData[0].data[1][0])
                // cy.writeFile('cypress/fixtures/testdata.json', jsonData)
                // finally we write the assertion rule to check if that data matches the data we expected the excel file to have.
                //   expect(jsonData[0].data[0]).to.eqls(data);
            })

    })
})