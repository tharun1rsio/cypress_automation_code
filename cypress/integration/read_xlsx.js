// Sample code for data conversion from excel to json

let rowsLength;
describe('Data conversion from xcel to json', () => {
    before(() => {
        cy.task('readXlsx', { file: 'cypress/data/testdata.xlsx', sheet: "log_in " }).then((rows) => {
            rowsLength = rows.length;
            cy.writeFile("cypress/fixtures/testdata.json", { rows })
        })
    })
    it('Data Driven: Register User', () => {
        cy.fixture('testdata').then((data) => {
            for (let i = 0; i < rowsLength; i++) {
                    cy.log(data.rows[i].firstName);
                    cy.log(data.rows[i].lastName);
                    cy.writeFile('cypress/fixtures/sample.xlsx', data.rows[i].firstName)
                    cy.writeFile('cypress/fixtures/sample.xlsx', data.rows[i].lastName)
            }
        })
    })
})
