/// <reference types="cypress" />
import 'cypress-file-upload';
const loginData = require('../../fixtures/Login.json')
// const funderOnboard = require('../../fixtures/Funder_onbarding.json')
let rowsLength;

describe('Funder Creation', () => {
    before(() => {
        cy.task('readXlsx', { file: 'cypress/data/Onboarding-Regressiontesting.xlsx', sheet: "Funder_Onboard" }).then((rows) => {
            rowsLength = rows.length;
            cy.writeFile("cypress/fixtures/Funder_onbarding.json", { rows })
        })
    })
    it('Open the 1PS page', () => {
        cy.visit('/')
        Cypress.on('uncaught:exception', (err, runnable) => { return false; })
    })

    it('Navigate to Funder onboarding screen', () => {
        cy.Login(loginData.username, loginData.password)
        cy.fixture('Funder_onbarding').then((data) => {
            for (let i = 0; i < rowsLength; i++) {
                cy.get(':nth-child(1) > .dropdown-toggle')
                    .click()
                cy.get('.open > .dropdown-menu > :nth-child(4) > a')
                    .click()
                cy.get('a > .icon-container > .fa')
                    .click()
                cy.get('#companyName')
                    .type(data.rows[i].CompanyName)
                cy.get('#vatRegistrationNumber')
                    .type(data.rows[i].VATRegistrationnumber)
                if (data.rows[i].Logo === 'YES') {
                    cy.get('.col-md-3 > .form-group > .form-control')
                        .attachFile({ filePath: '../data/' + data.rows[i].AttachLogo })
                }
                // })
                // it('Enter company address details', () => {
                cy.get('#companyAddressLineOne')
                    .click()
                    .type(data.rows[i].companyAddress1)
                cy.get('#companyAddressLineTwo')
                    .click()
                    .type(data.rows[i].companyAddress2)
                cy.get('#companyAddressLineThree')
                    .click()
                    .type(data.rows[i].companyAddress3)
                cy.get('#companyAddressLineFour')
                    .click()
                    .type(data.rows[i].companyAddress4)
                cy.get('#companyPostCode')
                    .click()
                    .type(data.rows[i].postCode)

                if (data.rows[i].invAddSameAsCmpAdd === 'YES') {
                    cy.get('h6 > input')
                        .check()
                } else {
                    cy.get('#invoiceAddressLineOne')
                        .click()
                        .type(data.rows[i].invoiceAddress1)
                    cy.get('#invoiceAddressLineTwo')
                        .click()
                        .type(data.rows[i].invoiceAddress2)
                    cy.get('#invoiceAddressLineThree')
                        .click()
                        .type(data.rows[i].invoiceAddress3)
                    cy.get('#invoiceAddressLineFour')
                        .click()
                        .type(data.rows[i].invoiceAddress4)
                    cy.get('#invoicePostCode')
                        .click()
                        .type(data.rows[i].invoicePostCode)
                }
                cy.get('#registrationNumber')
                    .click()
                    .type(data.rows[i].registrationNumber)
                cy.get('#url')
                    .click()
                    .type(data.rows[i].invWbURL)
                cy.get('#contactNumber')
                    .click()
                    .type(data.rows[i].contactNumber)
                cy.get('#emailId')
                    .click()
                    .type(data.rows[i].eMailId)
                // });
                // it('Enter invoice address details', () => {

                // });
                // it('Entr payable bank details', () => {
                cy.get('#bankNameHolder > .form-control')
                    .click()
                    .type(data.rows[i].bankNameHolder)
                cy.get('#accountHolderNameHolder > .form-control')
                    .click()
                    .type(data.rows[i].accountHolderNameHolder)
                cy.get('#accountNumberHolder > .form-control')
                    .click()
                    .type(data.rows[i].accountNumberHolder)
                cy.get('#sortCodeHolder > .form-control')
                    .click()
                    .type(data.rows[i].sortCodeHolder)
                cy.get('#assignmentNoticeHolder > .form-control')
                    .click()
                    .type(data.rows[i].assignmentNoice)
                cy.get('#ibanNumberHolder > .form-control')
                    .click()
                    .type(data.rows[i].ibanNumberHolder)
                cy.get('#swiftCodeHolder > .form-control')
                    .click()
                    .type(data.rows[i].swiftCodeHolder)
                // });
                // it('Save the entered details', () => {
                cy.get('#Button')
                .click()
                .wait(10000)
            }
        })
    });
})