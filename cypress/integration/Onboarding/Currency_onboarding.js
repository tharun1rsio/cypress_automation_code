/// <reference types="cypress" />
const loginData = require('../../fixtures/Login.json')
// const currencyOnboard = require('../../fixtures/Currency_onboarding.json')
let rowsLength;

describe('Currency Creation', () => {
    before(() => {
        cy.task('readXlsx', { file: 'cypress/data/Onboarding-Regressiontesting.xlsx', sheet: "Currency_Onboard" }).then((rows) => {
            rowsLength = rows.length;
            cy.writeFile("cypress/fixtures/Currency_onboarding.json", { rows })
        })
    })
    it('Open the 1PS page', () => {
        cy.visit('/')
        Cypress.on('uncaught:exception', (err, runnable) => { return false; })
    })

    it('1PS login', () => {
        cy.Login(loginData.username, loginData.password)
    })

    it('Navigate to Currency onboarding screen', () => {
        cy.fixture('Currency_onboarding').then((data) => {
            for (let i = 0; i < rowsLength; i++) {
                cy.get(':nth-child(1) > .dropdown-toggle')
                    .click()
                cy.get('.open > .dropdown-menu > :nth-child(5) > a')
                    .click()
                cy.get('a > .icon-container > .fa')
                    .click()
                // })
                // it('Enter currency information details', () => {
                    cy.log(data.rows[i].currency)
                cy.get('#currency')
                    .select(data.rows[i].currency)
                    .type('{enter}')
                cy.get('#taxName')
                    .select(data.rows[i].taxName)
                    .type('{enter}')
                // });
                // it('Enter tax percentage details', () => {
                cy.get('#percentage > .form-control')
                    .click()
                    .type(data.rows[i].percentage)
                // });
                // it('Save the entered details', () => {
                cy.get('#Button')
                .click()
                // cy.end()
            }
        })
    });
})