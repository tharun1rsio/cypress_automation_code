/// <reference types="cypress" />
import 'cypress-file-upload';
const loginData = require('../../fixtures/Login.json')
// const agencyOnboard = require('../../fixtures/Agency_onboarding.json')
let rowsLength;
describe('Agency Creation', () => {
    before(() => {
        cy.task('readXlsx', { file: 'cypress/data/Onboarding-Regressiontesting.xlsx', sheet: "Agency_Onboard" }).then((rows) => {
            rowsLength = rows.length;
            cy.writeFile("cypress/fixtures/Agency_onboarding.json", { rows })
        })
    })
    it('Open the 1PS page', () => {
        cy.visit('/')
        Cypress.on('uncaught:exception', (err, runnable) => { return false; })
    })
    it('Navigate to Agency onboarding screen', () => {
        cy.Login(loginData.username, loginData.password)
        cy.fixture('Agency_onboarding').then((data) => {
            for (let i = 0; i < rowsLength; i++) {
                
                cy.get(':nth-child(1) > .dropdown-toggle')
                    .click()
                cy.get('.open > .dropdown-menu > :nth-child(3) > a')
                    .click()
                cy.get('a > .icon-container > .fa')
                    .click()
                // })
                // it('Enter agency details', () => {

                cy.get('#companyName')
                    .click()
                    .type(data.rows[i].companyName)
                cy.get('#emailId')
                cy.log(data.rows[i].companyMail)
                    .click()
                    .type(data.rows[i].companyMail)

                // })
                // }); 
                // it('Enter company address', () => {
                cy.get('#companyAddressLineOne')
                    .click()
                    .type(data.rows[i].companyAddress1)
                cy.get('#companyAddressLineTwo  ')
                    .click()
                    .type(data.rows[i].companyAddress2)
                cy.get('#companyAddressLineThree    ')
                    .click()
                    .type(data.rows[i].companyAddress3)
                cy.get('#companyAddressLineFour ')
                    .click()
                    .type(data.rows[i].companyAddress4)
                cy.get('#companyPostCode')
                    .click()
                    .type(data.rows[i].postCode)
                if (data.rows[i].Logo === 'YES') {
                    cy.get('#logo')
                        .attachFile({ filePath: '../data/' + data.rows[i].AttachLogo })
                }
                // });
                // it('Enter invoice address', () => {
                if (data.rows[i].invAddSameAsCmpAdd === 'YES') {
                    cy.get('h6 > input')
                        .check()
                } else {
                    cy.get('#invoiceAddressLineOne')
                        .click()
                        .type(data.rows[i].invoiceAddress1)
                    cy.get('#invoiceAddressLineTwo')
                        .click()
                        .type(data.rows[i].invoiceAddress2)
                    cy.get('#invoiceAddressLineThree')
                        .click()
                        .type(data.rows[i].invoiceAddress3)
                    cy.get('#invoiceAddressLineFour')
                        .click()
                        .type(data.rows[i].invoiceAddress4)
                    cy.get('#invoicePostCode')
                        .click()
                        .type(data.rows[i].invoicePostCode)
                }
                cy.get('#vatRegistrationNumber')
                    .click()
                    .type(data.rows[i].vatRegistrationNumber)
                cy.get('#registrationNumber')
                    .click()
                    .type(data.rows[i].registrationNumber)
                cy.get('#url')
                    .click()
                    .type(data.rows[i].agyWbURL)
                cy.get('#contactNumber')
                    .click()
                    .type(data.rows[i].contactNumber)

                // });
                // it('Enter user information details', () => {
                cy.get('#invoiceManagerFirstName > .form-control')
                    .click()
                    .type(data.rows[i].agyuserFirstName)
                cy.get('#invoiceManagerLastName > .form-control')
                    .click()
                    .type(data.rows[i].agyuserLastName)
                cy.get('#invoiceManager > .form-control')
                    .click()
                    .type(data.rows[i].invContactNumber)
                cy.get('#invoiceManagerMailId > .form-control')
                    .click()
                    .type(data.rows[i].agyuserMail)
                cy.get('.form-group > #active')
                    .select(data.rows[i].activeStatus)
                    .type('{enter}')
                cy.get('.form-group > #marketingFlag')
                    .select(data.rows[i].marketing)
                    .type('{enter}')
                // });
                // it('Entr payable bank details', () => {
                cy.get('#bankNameHolder > .form-control')
                    .click()
                    .type(data.rows[i].bankNameHolder)
                cy.get('#accountHolderNameHolder > .form-control')
                    .click()
                    .type(data.rows[i].accountHolderNameHolder)
                cy.get('#accountNumberHolder > .form-control')
                    .click()
                    .type(data.rows[i].accountNumberHolder)
                cy.get('#sortCodeHolder > .form-control')
                    .click()
                    .type(data.rows[i].sortCodeHolder)
                cy.get('#assignmentNoticeHolder > .form-control')
                    .click()
                    .type(data.rows[i].assignmentNoice)
                cy.get('#ibanNumberHolder > .form-control')
                    .click()
                    .type(data.rows[i].ibanNumberHolder)
                cy.get('#swiftCodeHolder > .form-control')
                    .click()
                    .type(data.rows[i].swiftCodeHolder)
                // });
                // it('Enter 1PS service charge details', () => {
                cy.get('#onepsServiceChargePercentage')
                    .click()
                    .type(data.rows[i].onePsServiceCharge)
                // Commented for dr region only using in dev region only
                // cy.get('.form-group > #paidWhenPaidFee')
                //     .click()
                //     .type(data.rows[i].paidWhenPaidFee)
                // cy.get('.form-group > #permFee')
                //     .click()
                //     .type(data.rows[i].permFee)
                // });
                // it('Enter 1PS staff details', () => {
                cy.get('#salesStaffPrimary')
                    .select(data.rows[i].primaryStaff)
                    .type('{enter}')
                cy.get('#onboardingStaffPrimary')
                    .select(data.rows[i].onboardStaffPrimary)
                    .type('{enter}')
                cy.get('#creditControlStaffPrimary')
                    .select(data.rows[i].creditCtrlPrimary)
                    .type('{enter}')
                cy.get('#salesStaffSecondary')
                    .select(data.rows[i].secondaryStaff)
                    .type('{enter}')
                cy.get('#onboardingStaffSecondary')
                    .select(data.rows[i].onboardStaffSecondary)
                    .type('{enter}')
                cy.get('#creditControlStaffSecondary')
                    .select(data.rows[i].creditCtrlSecondary)
                    .type('{enter}')
                // });
                // it('Enter collection & support details', () => {
                cy.get('#operationSupport > #status-filter')
                    .select(data.rows[i].lvlOperationsupport)
                    .type('{enter}')
                cy.get('#collectionResource > #status-filter')
                    .select(data.rows[i].lvlcollectionReport)
                    .type('{enter}')
                // });
                // it('Save the entered details', () => {
                cy.get('#Button')
                // .click()
                .wait(10000)
            }
        });
    })
})