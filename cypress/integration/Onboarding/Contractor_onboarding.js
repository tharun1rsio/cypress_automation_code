/// <reference types="cypress" />
import 'cypress-file-upload';
const loginData = require('../../fixtures/Login.json')
// const contractorOnboard = require('../../fixtures/Contractor_onboarding.json')
let rowsLength;

describe('Contractor Creation', () => {
    before(() => {
        cy.task('readXlsx', { file: 'cypress/data/Onboarding-Regressiontesting.xlsx', sheet: "Contractor_Onboard" }).then((rows) => {
            rowsLength = rows.length;
            cy.writeFile("cypress/fixtures/Contractor_onboarding.json", { rows })
        })
    })
    it('Open the 1PS page', () => {
        cy.visit('/')
        Cypress.on('uncaught:exception', (err, runnable) => { return false; })
    })

    it('Navigate to Contractor onboarding screen', () => {
        cy.Login(loginData.username, loginData.password)
        cy.fixture('Contractor_onboarding').then((data) => {
            for (let i = 0; i < rowsLength; i++) {
                cy.get(':nth-child(1) > .dropdown-toggle')
                    .click()
                cy.get('.open > .dropdown-menu > :nth-child(2) > a')
                    .click()
                cy.get('a > .icon-container > .fa')
                    .click()
                // })
                // it('Enter contractor information', () => {
                cy.get('#firstName')
                    .click()
                    .type(data.rows[i].firstName)
                cy.get('#lastName')
                    .click()
                    .type(data.rows[i].lastName)
                cy.get('#email')
                    .click()
                    .type(data.rows[i].eMail)
                cy.get('#mobileNumber')
                    .click()
                    .type(data.rows[i].mobileNumber)
                cy.get('.form-group > #active')
                    .select(data.rows[i].activeStatus)
                    .type('{enter}')
                cy.get('#isUmbrellaCompany')
                    .select(data.rows[i].companyLimited)
                    .type('{enter}')
                if (data.rows[i].companyLimited == 'Umbrella Company') {
                    cy.get('#searchTextBox')
                        .click()
                        .type(data.rows[i].umbrellaCompanyName)
                        .get('#responseListItem0')
                        .click()
                } else {
                    cy.get('#companyName')
                        .click()
                        .type(data.rows[i].lmCompanyName)
                    cy.get('#paymentTerms')
                        .select(data.rows[i].lmPaymentterms)
                        .type('{enter}')
                    cy.get('#companyAddressLineOne')
                        .click()
                        .type(data.rows[i].companyAddress1)
                    cy.get('#companyAddressLineTwo')
                        .click()
                        .type(data.rows[i].companyAddress2)
                    cy.get('#companyAddressLineThree')
                        .click()
                        .type(data.rows[i].companyAddress3)
                    // cy.get('#companyAddressLineThree')
                    //     .click()
                    //     .type(data.rows[i].companyAddress4)
                    cy.get('#companyPostCode')
                        .click()
                        .type(data.rows[i].postCode)
                    // });
                    // it('Enter invoice address details', () => {
                    if (data.rows[i].invAddSameAsCmpAdd === 'YES') {
                        cy.get('h6 > input')
                            .check()
                    } else {
                        cy.get('#invoiceAddressLineOne')
                            .click()
                            .type(data.rows[i].invoiceAddress1)
                        cy.get('#invoiceAddressLineTwo')
                            .click()
                            .type(data.rows[i].invoiceAddress2)
                        cy.get('#companyAddressLineThree')
                            .click()
                            .type(data.rows[i].invoiceAddress3)
                        cy.get('#invoiceAddressLineFour')
                            .click()
                            .type(data.rows[i].invoiceAddress4)
                        // cy.get('#invoiceAddressLineFour')
                        //     .click()
                        //     .type(data.rows[i].invoiceAddress4)
                        cy.get('#invoicePostCode')
                            .click()
                            .type(data.rows[i].invoicePostCode)
                    }
                    cy.get('#registrationNumber')
                        .click()
                        .type(data.rows[i].registrationNumber)
                    cy.get('#vatRegistrationNumber')
                        .click()
                        .type(data.rows[i].vatRegistrationNumber)
                    cy.get('#url')
                        .click()
                        .type(data.rows[i].wbURL)
                    if (data.rows[i].Logo === 'YES') {
                        cy.get('#logo')
                            .attachFile({ filePath: '../data/' + data.rows[i].AttachLogo })
                    }
                    // });
                    // it('Currency and tax', () => {
                    cy.get('#searchTextBox')
                        .click()
                        .type(data.rows[i].currency)
                        .get('#responseListItem0')
                        .click()
                    cy.log(data.rows[i].tax)
                    cy.get('#currencyPercentage0 > .form-control')
                        .select(data.rows[i].tax)
                        .type('{enter}')
                    // .pause()
                    // });
                    // it('Entr payable bank details', () => {
                    cy.get('#bankNameHolder > .form-control')
                        .click()
                        .type(data.rows[i].bankNameHolder)
                    cy.get('#accountHolderNameHolder > .form-control')
                        .click()
                        .type(data.rows[i].accountHolderNameHolder)
                    cy.get('#accountNumberHolder > .form-control')
                        .click()
                        .type(data.rows[i].accountNumberHolder)
                    cy.get('#sortCodeHolder > .form-control')
                        .click()
                        .type(data.rows[i].sortCodeHolder)
                    cy.get('#ibanNumberHolder > .form-control')
                        .click()
                        .type(data.rows[i].ibanNumberHolder)
                    cy.get('#swiftCodeHolder > .form-control')
                        .click()
                        .type(data.rows[i].swiftCodeHolder)
                }
                cy.get('#Button')
                    .click()
                    .wait(10000)
            }
        })
    });
})