/// <reference types="cypress" />
import 'cypress-file-upload';
const loginData = require('../../fixtures/Login.json')
// const clientOnboard = require('../../fixtures/Client_onboarding.json')
let rowsLength;
describe('Client Creation', () => {
    before(() => {
        cy.task('readXlsx', { file: 'cypress/data/Onboarding-Regressiontesting.xlsx', sheet: "Client_Onboard" }).then((rows) => {
            rowsLength = rows.length;
            cy.writeFile("cypress/fixtures/Client_onboarding.json", { rows })
        })
    })
    it('Open the 1PS page', () => {
        cy.visit('/')
        Cypress.on('uncaught:exception', (err, runnable) => { return false; })
    })

    it('Navigate to Client onboarding screen', () => {
        cy.Login(loginData.username, loginData.password)
        cy.fixture('Client_onboarding').then((data) => {
            for (let i = 0; i < rowsLength; i++) {
                cy.get(':nth-child(1) > .dropdown-toggle')
                    .click()
                cy.get('.open > .dropdown-menu > :nth-child(1) > a')
                    .click()
                cy.get('a > .icon-container > .fa')
                    .click()
                // })
                // it('Enter client details', () => {
                cy.get('#companyName')
                    .click()
                    .type(data.rows[i].companyName)
                cy.get('[name="mailId"]')
                    .click()
                    .type(data.rows[i].companyMail)
                cy.get('#industryType')
                    .select(data.rows[i].industryType)
                    .type('{enter}')
                // });
                // it('Enter company address', () => {
                cy.get('input[name="companyAddressLineOne"]')
                    .click()
                    .type(data.rows[i].companyAddress1)
                cy.get('input[name="companyAddressLineTwo"]')
                    .click()
                    .type(data.rows[i].companyAddress2)
                cy.get('input[name="companyAddressLineThree"]')
                    .click()
                    .type(data.rows[i].companyAddress3)
                cy.get('input[name="companyAddressLineFour"]')
                    .click()
                    .type(data.rows[i].companyAddress4)
                cy.get('input[name="companyPostCode"]')
                    .click()
                    .type(data.rows[i].postCode)
                if (data.rows[i].CmpyLogo === 'YES') {
                    cy.get('#logo')
                        .attachFile({ filePath: '../data/' + data.rows[i].CmpyAttachLogo });
                }

                if (data.rows[i].invAddSameAsCmpAdd === 'YES') {
                    cy.get('h6 > input')
                        .check()
                } else {
                    cy.get('input[name="invoiceAddressLineOne"]')
                        .click()
                        .type(data.rows[i].invoiceAddress1)
                    cy.get('input[name="invoiceAddressLineTwo"]')
                        .click()
                        .type(data.rows[i].invoiceAddress2)
                    cy.get('input[name="invoiceAddressLineThree"]')
                        .click()
                        .type(data.rows[i].invoiceAddress3)
                    cy.get('input[name="invoiceAddressLineFour"]')
                        .click()
                        .type(data.rows[i].invoiceAddress4)
                    cy.get('input[name="companyPostCode"]')
                        .click()
                        .type(data.rows[i].invoicePostCode)
                }
                cy.get('input[name="vatRegistrationNumber"]')
                    .click()
                    .type(data.rows[i].vatRegistrationNumber)
                cy.get('input[name="registrationNumber"]')
                    .click()
                    .type(data.rows[i].registrationNumber)
                cy.get('input[name="url"]')
                    .click()
                    .type(data.rows[i].invWbURL)
                if (data.rows[i].InvLogo === 'YES') {
                    cy.get('#logo1')
                        .attachFile({ filePath: '../data/' + data.rows[i].InvAttachLogo });
                }
                // });
                // it('Enter invoice address', () => {

                // });
                // it('Enter hiring manager', () => {
                cy.get('.RecruitersRepeatSec > .row > :nth-child(1) > #recruiterFirstName0 > .form-control')
                    .click()
                    .type(data.rows[i].hmFirstName)
                cy.get('.RecruitersRepeatSec > .row > :nth-child(2) > #recruiterLastName0 > .form-control')
                    .click()
                    .type(data.rows[i].hmLastName)
                cy.get('.RecruitersRepeatSec > .row > :nth-child(3) > #recruitingManager0 > .form-control')
                    .click()
                    .type(data.rows[i].hmNumber)
                cy.get('.RecruitersRepeatSec > .row > :nth-child(4) > #recruiterMailid0 > .form-control')
                    .click()
                    .type(data.rows[i].hmMail)
                cy.get(':nth-child(10) > .RecruitersRepeatSec > .row > :nth-child(5) > #active0 > #active')
                    .select(data.rows[i].hmActive)
                    .type('{enter}')
                // });
                // it('Enter timesheet approver', () => {
                cy.get('.TimesheetRepeatSec > .row > :nth-child(1) > #recruiterFirstName0 > .form-control')
                    .click()
                    .type(data.rows[i].tsFirstName)
                cy.get('.TimesheetRepeatSec > .row > :nth-child(2) > #recruiterLastName0 > .form-control')
                    .click()
                    .type(data.rows[i].tsLastName)
                cy.get('.TimesheetRepeatSec > .row > :nth-child(3) > #recruitingManager0 > .form-control')
                    .click()
                    .type(data.rows[i].tsNumber)
                cy.get('.TimesheetRepeatSec > .row > :nth-child(4) > #recruiterMailid0 > .form-control')
                    .click()
                    .type(data.rows[i].tsMail)
                cy.get('.TimesheetRepeatSec > .row > :nth-child(5) > #active0 > #active')
                    .select(data.rows[i].tsActive)
                    .type('{enter}')
                // });
                // it('Enter invoice contact', () => {
                //     cy.get(':nth-child(14) > .create-add-row > a')
                //     .click()
                //     cy.get('#invoiceManagerFirstName > .form-control')
                //         .click()
                //         .type(data.rows[i].tsFirstName)
                //         cy.get('#invoiceManagerLastName > .form-control')
                //         .click()
                //         .type(data.rows[i].tsLastName)
                //     cy.getcy.get('#invoiceManagerMobileNumber > .form-control')
                //         .click()
                //         .type(data.rows[i].tsNumber)
                //     cy.getcy.get('#invoiceManagerMailId > .form-control')
                //         .click()
                //         .type(data.rows[i].tsMail)
                //         cy.get('.InvoiceContRepeatSec > .row > :nth-child(5) > .form-group > #active')
                //         .select(data.rows[i].tsActive)
                //         .type('{enter}')
                // });
                // it('Enter client full approver', () => {
                cy.get('#clientFullApproverFirstName > .form-control')
                    .click()
                    .type(data.rows[i].cfaFirstName)
                cy.get('#clientFullApproverLastName > .form-control')
                    .click()
                    .type(data.rows[i].cfaLastName)
                cy.get('#clientFullApprover > .form-control')
                    .click()
                    .type(data.rows[i].cfaNumber)
                cy.get('#clientFullApproverMailId > .form-control')
                    .click()
                    .type(data.rows[i].cfaMail)
                cy.get(':nth-child(16) > .RecruitersRepeatSec > .row > :nth-child(5) > #active0 > #active')
                    .select(data.rows[i].cfaActive)
                    .type('{enter}')
                // });
                // it('Enter client admin approver', () => {
                cy.get('#clientAdminFirstName > .form-control')
                    .click()
                    .type(data.rows[i].caFirstName)
                cy.get('#clientAdminLastName > .form-control')
                    .click()
                    .type(data.rows[i].caLastName)
                cy.get('#clientAdmin > .form-control')
                    .click()
                    .type(data.rows[i].caNumber)
                cy.get('#clientAdminMailId > .form-control')
                    .click()
                    .type(data.rows[i].caMail)
                cy.get(':nth-child(18) > .RecruitersRepeatSec > .row > :nth-child(5) > #active0 > #active')
                    .select(data.rows[i].caActive)
                    .type('{enter}')
                // });
                // it('Enter credit control details', () => {
                cy.get('#creditLimitAmount')
                    .click()
                    .type(data.rows[i].creditLimitAmount)
                cy.get('#creditLimitReference')
                    .click()
                    .type(data.rows[i].creditLimitRef)
                cy.get('#badDebtLimitAmount')
                    .click()
                    .type(data.rows[i].badDebtAmount)
                cy.get('#badDebtLimitReference')
                    .type(data.rows[i].badDebtLimitRef)
                // });
                // it('Enter 1PS staff details', () => {
                cy.get('#onboardingStaffPrimary')
                    .select(data.rows[i].primaryStaff)
                    .type('{enter}')
                cy.get('#creditControlStaffPrimary')
                    .select(data.rows[i].creditCtrlPrimary)
                    .type('{enter}')
                cy.get('#onboardingStaffSecondary')
                    .select(data.rows[i].secondaryStaff)
                    .type('{enter}')
                cy.get('#creditControlStaffSecondary')
                    .select(data.rows[i].creditCtrlSecondary)
                    .type('{enter}')
                // });
                // it('Save the entered details', () => {
                cy.get('#Button')
                .click()
                .wait(10000)
                // cy.end()
            }
        });
    })

})