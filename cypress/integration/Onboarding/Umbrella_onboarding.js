/// <reference types="cypress" />
import 'cypress-file-upload';
const loginData = require('../../fixtures/Login.json')
// const Umbrella = require('../../fixtures/Umbrella_onboarding.json')
let rowsLength;

describe('Umbrella creation', () => {
    before(() => {
        cy.task('readXlsx', { file: 'cypress/data/Onboarding-Regressiontesting.xlsx', sheet: "Umbrella_Onboard" }).then((rows) => {
            rowsLength = rows.length;
            cy.writeFile("cypress/fixtures/Umbrella_onboarding.json", { rows })
        })
    })
    it('Open the 1PS page', () => {
        cy.visit('/')
        Cypress.on('uncaught:exception', (err, runnable) => { return false; })
    })

    it('1PS login', () => {
        cy.Login(loginData.username, loginData.password)
    })
    it('Navigate to umbrella onboarding screen', () => {
        cy.fixture('Umbrella_onboarding').then((data) => {
            for (let i = 0; i < rowsLength; i++) {
                cy.get(':nth-child(1) > .dropdown-toggle')
                    .click()
                cy.get('.dropdown-menu > :nth-child(6) > a')
                    .click()
                cy.get('a > .icon-container > .fa')
                    .click()
                // cy.pause()
                // })
                // it('Enter company information details', () => {
                cy.get('#companyName')
                    .type(data.rows[i].companyName)
                cy.get('#paymentTerms')
                    .type(data.rows[i].paymentTerms)
                // })
                // it('Enter company address details', () => {
                cy.get('#companyAddressLineOne')
                    .type(data.rows[i].companyAddressLine1)
                cy.get('#companyAddressLineTwo')
                    .type(data.rows[i].companyAddressLine2)
                cy.get('#companyAddressLineThree')
                    .type(data.rows[i].companyAddressLine3)
                cy.get('#companyAddressLineFour')
                    .type(data.rows[i].companyAddressLine4)
                cy.get('#companyPostCode')
                    .type(data.rows[i].companyPostCode)

                if (data.rows[i].invAddSameAsCmpAdd === 'YES') {
                    cy.get('h6 > input')
                        .check()
                } else {
                    cy.get('#invoiceAddressLineOne')
                        .type(data.rows[i].invoiceAddressLine1)
                    cy.get('#invoiceAddressLineTwo')
                        .type(data.rows[i].invoiceAddressLine2)
                    cy.get('#invoiceAddressLineThree')
                        .type(data.rows[i].invoiceAddressLine3)
                    cy.get('#invoiceAddressLineFour')
                        .type(data.rows[i].invoiceAddressLine4)
                    cy.get('#invoicePostCode')
                        .type(data.rows[i].invoicePostCode)
                }
                cy.get('#registrationNumber')
                    .type(data.rows[i].registrationNumber)
                if (data.rows[i].VATno === 'YES') {
                    cy.get('#vatRegistrationNumber')
                        .type(data.rows[i].vatRegistrationNumber)
                }
                cy.get('#url')
                    .type(data.rows[i].url)
                cy.get('#contactNumber')
                    .type(data.rows[i].contactNumber)
                cy.get('#emailId')
                    .type(data.rows[i].emailId)
                if (data.rows[i].Logo === 'YES') {
                    cy.get('#logo')
                        .attachFile({ filePath: '../data/' + data.rows[i].AttachLogo })
                }

                // })
                // it('Enter invoice address details', () => {

                // })
                // it('Enter Currency and Tax', () => {
                cy.get('#searchTextBox')
                    .click()
                    .type(data.rows[i].Searchcurrency)
                    .get('#responseListItem0')
                    .click()
                cy.get('#currencyPercentage0 > .form-control')
                    .select(data.rows[i].Tax)
                // })
                // it('Enter bank detail', () => {
                cy.get('[name="bankName"]')
                    .type(data.rows[i].Bankname)
                cy.get('[name="accountHolderName"]')
                    .type(data.rows[i].accountHolderName)
                cy.get('[name="accountNumber"]')
                    .type(data.rows[i].accountNumber)
                cy.get('[name="sortCode"]')
                    .type(data.rows[i].sortCode)
                cy.get('[name="ibanNumber"]')
                    .type(data.rows[i].ibanNumber)
                cy.get('[name="swiftCode"]')
                    .type(data.rows[i].swiftcode)
                // })
                // it('Save the entered details', () => {
                cy.get('#Button')
                .click()
                .wait(10000)
                // cy.end()
            }
        })
    });
})