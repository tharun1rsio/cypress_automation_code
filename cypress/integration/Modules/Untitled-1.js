/// <reference types="Cypress" />

describe("2.0 Login page", () => {
	it("Visits the 1PS 2.0 login page", () => {
		cy.visit("http://localhost:3000/");
	});

	it('Enter the log in username', () => {
		cy.get('.p-4.bg-gray-300.rounded-md.relative.flex.flex-col.min-w-0.mb-6.shadow-md > :nth-child(1)')
			.type('jdavies@my1ps.com')
	});
	it('Enter the log in password', () => {
		cy.get('[type="password"]')
			.type('Platform.1')
	});
	it('click the log in button', () => {
		cy.get('button')
			.contains('Sign In')
			.click()
			.wait(5000)
	});
	it('session storgage value', () => {
		cy.log(sessionStorage.getItem('jwt'))
	});
	let accesstoken = sessionStorage.getItem('jwt')
	it('get user', () => {
		cy.request({
			url: 'http://localhost:3000/api/sentinel/query',
			method: 'POST',
			headers: {
				authorization: accesstoken
			},
			// Users query
			// body: { "variables": {}, "graphql": [{ "module": "platform", "entity": "user", "fields": { "id": true, "updated_at": true, "sid": true, "name": true, "email": true }, "alias": "records", "where": {}, "filters": { "limit": 10, "offset": 0, "order_by": {} } }, { "module": "platform", "entity": "user", "fields": { "id": true, "updated_at": true, "sid": true, "name": true, "email": true }, "alias": "total", "where": {}, "aggregate": { "count": true } }, { "module": "platform", "entity": "user", "fields": { "id": true, "updated_at": true, "sid": true, "name": true, "email": true }, "alias": "count", "where": {}, "aggregate": { "count": true } }] }
			// Placements query
			body: { "variables": {}, "graphql": [{ "module": "fundo", "entity": "placement", "fields": { "id": true, "sid": true, "updated_at": true, "agency_name": true, "client_name": true, "start_date": true, "end_date": true, "contractor": { "fields": { "id": true, "name": true } }, "timesheet": { "aggregate": { "count": true } } }, "alias": "records", "where": {}, "filters": { "limit": 10, "offset": 0, "order_by": { "sid": "desc_nulls_last" } } }, { "module": "fundo", "entity": "placement", "fields": { "id": true, "sid": true, "updated_at": true, "agency_name": true, "client_name": true, "start_date": true, "end_date": true, "contractor": { "fields": { "id": true, "name": true } }, "timesheet": { "aggregate": { "count": true } } }, "alias": "total", "where": {}, "aggregate": { "count": true } }, { "module": "fundo", "entity": "placement", "fields": { "id": true, "sid": true, "updated_at": true, "agency_name": true, "client_name": true, "start_date": true, "end_date": true, "contractor": { "fields": { "id": true, "name": true } }, "timesheet": { "aggregate": { "count": true } } }, "alias": "count", "where": {}, "aggregate": { "count": true } }] }
		})
			.should((response) => {
				cy.log(JSON.stringify(response.body.data.records))//print the datat in the log screen
				// });
				// const apiResponse = response.body.data.records

				cy.visit('http://localhost:3000/fundo/placement/list')
				cy.get('tbody > :nth-child(1) > :nth-child(2)').contains(response.body.data.records[1].sid);
				cy.get('tbody > :nth-child(1) > :nth-child(3)').contains(response.body.data.records[1].agency_name);
				cy.get('tbody > :nth-child(1) > :nth-child(4)').contains(response.body.data.records[1].client_name);
				// cy.get('tbody > :nth-child(1) > :nth-child(5)').contains(response.body.data.records[1].contractor);
				cy.get('tbody > :nth-child(1) > :nth-child(6)').contains(response.body.data.records[1].start_date);
				cy.get('tbody > :nth-child(1) > :nth-child(7)').contains(response.body.data.records[1].end_date);
				// cy.get('tbody > :nth-child(1) > :nth-child(8)').contains(response.body.data.records[1].timesheets);
			})
	})

})