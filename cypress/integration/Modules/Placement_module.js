/// <reference types="cypress" />
const loginData = require('../../fixtures/Login.json')
// const Placement = require('../../fixtures/Placement_module.json')
let rowsLength;

describe('Placement Creation', () => {
    before(() => {
        cy.task('readXlsx', { file: 'cypress/data/Placementmodule -RTesting.xlsx', sheet: "Placement_Creation" }).then((rows) => {
            rowsLength = rows.length;
            cy.writeFile("cypress/fixtures/Placement_module.json", { rows })
        })
    })
    it('Open the 1PS page', () => {
        cy.visit('/')
        Cypress.on('uncaught:exception', (err, runnable) => { return false; })
    })

    it('Navigate to placement creation screen', () => {
        cy.Login(loginData.username, loginData.password)
        cy.get(':nth-child(2) > a > p')
            .click()
        cy.fixture('Placement_module').then((data) => {
            for (let i = 0; i < rowsLength; i++) {
                cy.get('a > .icon-container > .fa')
                    .click()
                    .wait(3000)
                // })
                // it('Enter client information details', () => {
                    cy.get(':nth-child(9) > :nth-child(2) > :nth-child(1) > .form-group > :nth-child(2) > .input-group > #searchTextBox')
                    .click()
                    .type(data.rows[i].contractorName)
                    // .wait(1000)
                    .pause()
                    .get('#responseListItem1')
                    .click()
                    .wait(2000)
                    cy.log(data.rows[i].contractorCompanyMail)
                cy.get('#companyEmail')
                    .should('have.value',data.rows[i].contractorCompanyMail)
                cy.get('#searchTextBox')
                    .click()
                    .type(data.rows[i].companyName)
                    .get('#responseListItem0')
                    .click()
                    .wait(2000)
                    .pause()
                    
                // cy.get('#registeredAddress')
                //     .should('have.text')
                // cy.get('#invoicingAddress')
                //     .should('have.text')
                cy.get('#isTimesheetFlag')
                    .select(data.rows[i].timesheetFlag)
                    .type('{enter}')
                // });
                // it('Enter hiring manager details', () => {
                cy.get('#recruitingManager')
                    .select(data.rows[i].hiringManager)
                    .type('{enter}')
                cy.get('#recruitingManagerMail')
                    .contains(data.rows[i].hiringMail)
                // });
                // it('Enter timesheet approver details', () => {
                cy.get('#timesheetManager')
                    .select(data.rows[i].timesheetApprover)
                    .type('{enter}')
                cy.get('#timesheetManagerEmailId')
                    .contains(data.rows[i].timesheetApproverMail)
                // });
                // it('Enter invoice contact details', () => {
                //     cy.get('#invoiceContract')
                //         .select(data.rows[i].invoiceContact)
                //         .type('{enter}')
                //     cy.get('#invoiceContractMail')
                //         .should('have.text', data.rows[i].invoiceContactMail)
                // });
                // it('Enter Agency information details', () => {
                cy.get('#paymentTermsTextHolder > :nth-child(2) > .input-group > #searchTextBox')
                    .click()
                    .type(data.rows[i].agencyName)
                    .get('#responseListItem0')
                    .click()
                // commented for dr region test
                // cy.get('#expensesPaymentType')
                //     .select(data.rows[i].expensesPaymentType)
                //     .type('{enter}')
                // });
                // it('Enter currency and tax', () => {
                cy.get('#currencyundefined > :nth-child(2) > .input-group > #searchTextBox')
                    .click()
                    .type(data.rows[i].currency)
                    .get('#responseListItem0')
                    .click()
                cy.get('#currencyPercentageundefined > .form-control')
                    .select(data.rows[i].currencyTax)
                // });
                // it('Enter placement terms details', () => {
                cy.get('#timeSheetFrequency')
                    .select(data.rows[i].timesheetFrequency)
                    .type('{enter}')
                cy.get('#purchaseOrderNumber')
                    .click()
                    .type(data.rows[i].purchaseOrderNo)
                cy.get('#paymentTerms')
                    .select(data.rows[i].paymentTerm)
                    .type('{enter}')
                cy.get('#baseRate')
                    .click()
                    .type(data.rows[i].contractorRate)
                cy.get('#baseRateType')
                    .select(data.rows[i].contractorRateType)
                    .type('{enter}')
                cy.get('#hoursPerWeek')
                    .click()
                    .type(data.rows[i].stduUnitsPerWeek)
                cy.get('#baseAgencyCharge')
                    .click()
                    .type(data.rows[i].agencyCommision)
                cy.get('#baseAgencyChargeType')
                    .select(data.rows[i].commissionChargeType)
                cy.get('#clientNoticePeriod')
                    .click()
                    .type(data.rows[i].clientNoticePeriod)
                cy.get('#contractorNoticePeriod')
                    .click()
                    .type(data.rows[i].contractorNoticePeriod)
                // });
                // it('Enter contractor information details', () => {
                cy.get(':nth-child(9) > :nth-child(2) > :nth-child(1) > .form-group > :nth-child(2) > .input-group > #searchTextBox')
                    .click()
                    .type(data.rows[i].contractorName)
                    // .wait(1000)
                    .get('#responseListItem1')
                    .click()
                    .wait(2000)
                    cy.log(data.rows[i].contractorCompanyMail)
                cy.get('#companyEmail')
                    .contains(data.rows[i].contractorCompanyMail)
                // cy.get('#companyMobile')
                //     .contains(data.rows[i].contractorMobile)
                // cy.get('#companyName')
                //     .contains(data.rows[i].contactorCampanyName)
                // cy.get('#registerNumber')
                //     .contains(data.rows[i].contactorRegiterNo)
                // cy.get('#companyAddress')
                //     .contains(data.rows[i].contactorCompanyAddress)
                // cy.get('#companyInvoiceAddress')
                //     .contains(data.rows[i].contactorInvoiceAddress)
                // cy.get('#paymentTermsText')
                //     .contains(data.rows[i].paymentTerms)
                cy.get('#contractorRoleType')
                    .select(data.rows[i].roleType)
                // });
                // it('Enter position information', () => {
                cy.get('input[name="jobTitle"]')
                    .click()
                    .type(data.rows[i].jobTitle)
                cy.get('input[id="startDate"]')
                    .click()
                    .type(data.rows[i].startDate)
                cy.get('input[id="endDate"]')
                    .click()
                    .type(data.rows[i].endDate)
                cy.get('#employingEntity')
                    .select(data.rows[i].employeeEntity)
                cy.get('div > [value="' + data.rows[i].IR35determination + '"]')
                    .check()
                if (data.rows[i].sendToAgency === true) {
                    cy.get('#sendToButton')
                    // .click()
                }
                if (data.rows[i].sendToClient === true) {
                    cy.get('#Button')
                    // .click()
                }
            }
        })
    });
})