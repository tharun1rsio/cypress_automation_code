/// <reference types="cypress" />
import dateformat from 'dateformat';
const DATEFORMAT = "yyyy-mm-dd"
const loginData = require('../../fixtures/Login.json')
const Timesheet = require('../../fixtures/Timesheet_module.json')
// function calculateTotalHours() {
//     var unitsDefault = 0;
//     for (var obj in this.state.workingHours) {
//         if (this.state.workingHours[obj].units !== "") {
//             unitsDefault = unitsDefault + parseFloat(this.state.workingHours[obj].units);
//         }
//     }
// }
describe("Validate timesheet data", () => {
    it('Open the 1PS page', () => {
        cy.visit('/')
        Cypress.on('uncaught:exception', (err, runnable) => { return false; })
    })
    it('Navigate to TimeSheets and validate', () => {
        cy.Login(loginData.username, loginData.password)
        cy.get(':nth-child(3) > a > p')
            .click()
        cy.get('[data-toggle="modal"] > .fa')
            .click()
        cy.get('.card > :nth-child(2) > .form-group > .form-control')
            .type(Timesheet.plcRef)
        // cy.get(':nth-child(3) > .form-group > .form-control')
        //     .type(Timesheet.contractor)
        // cy.get(':nth-child(4) > .form-group > .form-control')
        //     .type(Timesheet.AgencyName)
        // cy.get(':nth-child(5) > .form-group > .form-control')
        //     .type(Timesheet.client)
        // cy.get('.search-wrapper')
        //     .click()
        //     .type(Timesheet.Status)
        //     .type('{enter}')
        cy.get('.modal-title > .material-icons')
            .click()
        cy.get('.btn-success')
            .click()
            .wait(5000)
        cy.get(':nth-child(1) > :nth-child(10) > a > span')
            .click()

        cy.getCookie('Authorization').then((Cookie) => {
            cy.log(Cookie.value)
            let accessToken = Cookie.value
            let apiBody = { "contractReference": "4461" }//placement no
            // cy.apiRequest('https://1ps-dev-api.my1ps.com/timesheet/search', 'POST', accessToken, apiBody).then((response) => {
            cy.apiRequest('https://dr-api.my1ps.com/timesheet/search', 'POST', accessToken, apiBody).then((response) => {
                cy.log(JSON.stringify(response.body.content))
                let timesheetNo = response.body.content
                // cy.pause()
                cy.apiRequest('https://dr-api.my1ps.com/timesheet/' + timesheetNo[0].id, 'GET', accessToken).then((response) => {
                    var startDate = dateformat(new Date(response.body.startDate), DATEFORMAT, true);
                    var endDate = dateformat(new Date(response.body.endDate), DATEFORMAT, true);
                    cy.get(':nth-child(1) > :nth-child(1) > .form-group > .form-control')
                        .should('have.value', startDate)
                    cy.get(':nth-child(1) > :nth-child(2) > .form-group > .form-control')
                        .should('have.value', endDate)
                    cy.log(JSON.stringify(response.body.timeSheetDetails))
                    for (var i = 0; i < response.body.timeSheetDetails.length; i++) {
                        var dates = dateformat(new Date(response.body.timeSheetDetails[i].date), DATEFORMAT, true);
                        var j = i + 1
                        function getDayName(dateStr, locale) {
                            var date = new Date(dateStr);
                            return date.toLocaleDateString(locale, { weekday: 'long' });
                        }
                        var day = getDayName(dates);
                        cy.get('.CreateTimesheetDtHrRepeatSec > :nth-child(' + j + ') > .row > :nth-child(1) > div > .form-control')
                            .should('have.value', dates)
                        cy.get('.CreateTimesheetDtHrRepeatSec > :nth-child(' + j + ') > .row > :nth-child(2) > div > .form-control')
                            .should('have.value', day)
                        cy.get('.CreateTimesheetDtHrRepeatSec > :nth-child(' + j + ') > .row > :nth-child(3) > div > .form-control')
                            .should('have.value', response.body.timeSheetDetails[i].units)
                        cy.get('.CreateTimesheetDtHrRepeatSec > :nth-child(' + j + ') > .row > :nth-child(4) > div > .form-control')
                            .should('have.value', response.body.timeSheetDetails[i].contractRate.rateName)
                    }
                    if (response.body.contract.contractRates.rateType === 1) {
                        let totalnounits = parseFloat(response.body.totalUnits).toFixed(2)
                        cy.log(totalnounits)
                        cy.get('.col-md-6 > .form-group > .form-control')
                            .should('have.value', totalnounits)
                    } else {
                        cy.get('.col-md-6 > .form-group > .form-control')
                            .should('have.value', response.body.totalUnits)
                    }
                    cy.get('.card-content > .text-center > .btn-primary')
                    .click()
                })

            })
            // cy.request({
            //     url: 'https://1ps-dev-api.my1ps.com/timesheet/' + Timesheet.TimesheetNo,
            //     method: 'GET',
            //     headers: {
            //         Authorization: accessToken
            //     },
            // })
            //     .then((response) => {

            //     })
        })
    })
})