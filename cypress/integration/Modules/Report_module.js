// <reference types="cypress" />
const loginData = require('../../fixtures/Login.json')
const Placement = require('../../fixtures/Placement_module.json')
const Report = require('../../fixtures/Report_module.json')

describe('Report Creation', () => {
    it('Open the 1PS page', () => {
        cy.visit('http://localhost:3000/')
        Cypress.on('uncaught:exception', (err, runnable) => { return false; })
    })
    it('1PS login', () => {
        cy.Login(loginData.username, loginData.password)
        cy.get(':nth-child(7) > a > p')
            .click()
    })
    it('Enter start date and end date', () => {
        cy.get(':nth-child(1) > .form-group > .form-control')
            .click()
            .type(Report.startDate)
        cy.get('.col-md-12 > :nth-child(2) > .form-group > .form-control')
            .click()
            .type(Report.endDate)
    });
    it('Enter details to get report for Agency', () => {
        if (Report.agencyCompanyName === true) {
            cy.get(':nth-child(4) > .border-div > .modal-body > .col-md-12 > .col-md-6 > :nth-child(2) > .input-group > #searchTextBox')
                .click()
                .type(Report.agencyName)
                .get('#responseListItem0')
                .click()
        }

        if (Report.ContractPayment === true) {
            cy.get(':nth-child(4) > .border-div > .modal-body > :nth-child(2) > :nth-child(1)')
                .click()
        }

        if (Report.salesExport === true) {
            cy.get('.border-div > .modal-body > :nth-child(2) > :nth-child(2)')
                .click()
        }

        if (Report.agedSalesLedgerByAgency === true) {
            cy.get('.border-div > .modal-body > :nth-child(2) > :nth-child(3)')
                .click()
        }

        if (Report.agencyCommissionReport === true) {
            cy.get('.border-div > .modal-body > :nth-child(2) > :nth-child(4)')
                .click()
        }

        if (Report.intermediatryReport === true) {
            cy.get('.modal-body > :nth-child(2) > :nth-child(5)')
                .click()
        }
    });
    it('Enter details to get report for client', () => {
        if (Report.clientCompanyName === true) {
            cy.get(':nth-child(6) > .border-div > .modal-body > .col-md-12 > .col-md-6 > :nth-child(2) > .input-group > #searchTextBox')
                .click()
                .type(Report.clientName)
                .get('#responseListItem0')
                .click()
        }

        if (Report.invToEndClient === true) {
            cy.get(':nth-child(6) > .border-div > .modal-body > :nth-child(2)')
                .click()
        }

        if (Report.agedSalesLedgerByClient === true) {
            cy.get(':nth-child(6) > .border-div > .modal-body > :nth-child(3)')
                .click()
        }

        if (Report.receivables === true) {
            cy.get('.modal-body > :nth-child(4)')
                .click()
        }
    });
    it('Enter details to get report for 1PS', () => {
        if (Report.onePSAgyCompanyName === true) {
            cy.get('.col-md-6 > :nth-child(2) > .input-group > #searchTextBox')
                .click()
                .type(Report.onePSAgencyName)
                .get('#responseListItem0')
                .click()
        }

        if (Report.onePSFees === true) {
            cy.get(':nth-child(8) > .border-div > .modal-body > :nth-child(2)')
                .click()
        }

        if (Report.weeklyPaymentReport === true) {
            cy.get(':nth-child(6) > .border-div > .modal-body > :nth-child(3)')
                .click()
        }
    });
    it('Enter details to get report for timesheets and expenses', () => {
        if (Report.tsAgencyName === true) {
            cy.get(':nth-child(10) > .border-div > .modal-body > :nth-child(1) > :nth-child(1) > :nth-child(2) > .input-group > #searchTextBox')
                .click()
                .type(Report.tsAgencyNameData)
                .get('#responseListItem0')
                .click()
        }

        if (Report.tsClientName === true) {
            cy.get(':nth-child(2) > :nth-child(2) > .input-group > #searchTextBox')
                .click()
                .type(Report.tsClientNameData)
                .get('#responseListItem0')
                .click()
        }

        if (Report.tsContractorName === true) {
            cy.get(':nth-child(3) > :nth-child(2) > .input-group > #searchTextBox')
                .click()
                .type(Report.tsContractorNameData)
                .get('#responseListItem0')
                .click()
        }

        if (Report.tsContractoIdName === true) {
            cy.get(':nth-child(1) > .input-group > #searchTextBox')
                .click()
                .type(Report.tsContractoIdNameData)
                .get('#responseListItem0')
                .click()
        }

        if (Report.tsStatus === true) {
            cy.get(':nth-child(1) > #status-filter')
                .click()
                .select(Report.tsStatusData)
        }

        if (Report.Expstatus === true) {
            cy.get(':nth-child(2) > #status-filter')
                .click()
                .select(Report.ExpstatusData)
        }

        if (Report.plcStatus === true) {
            cy.get(':nth-child(3) > #status-filter')
                .click()
                .select(Report.plcStatusData)
        }

        if (Report.submissionDate === true) {
            cy.get('.row > .col-md-12 > :nth-child(4) > .form-control')
                .click()
                .type(Report.submissionDateData)
        }

        if (Report.timesheetReport === true) {
            cy.get(':nth-child(10) > .border-div > .modal-body > :nth-child(2) > .row > .col-md-12 > :nth-child(1) > .btn')
                .click()
        }

        if (Report.expensesReport === true) {
            cy.get(':nth-child(2) > .col-md-3 > .btn')
                .click()
        }

        if (Report.placementReport === true) {
            cy.get(':nth-child(2) > .row > .col-md-12 > :nth-child(3)')
                .click()
        }
    });

    it('Enter details to get report for collection', () => {
        if (Report.collectionClientName === true) {
            cy.get(':nth-child(12) > .border-div > .modal-body > :nth-child(1) > :nth-child(1) > :nth-child(2) > .input-group > #searchTextBox')
                .click()
                .type(Report.collectionClientNameData)
                .get('#responseListItem0')
                .click()
        }

        if (Report.collectionCommunicationType === true) {
            cy.get('.modal-body > :nth-child(1) > :nth-child(2) > .form-control')
                .click()
                .select(Report.collectionCommunicationTypeData)
        }

        if (Report.collectionCommunictionSubject === true) {
            cy.get('.modal-body > :nth-child(1) > :nth-child(3) > .form-control')
                .click()
                .select(Report.collectionCommunictionSubjectData)
        }

        if (Report.collectionChasedBy === true) {
            cy.get('.modal-body > :nth-child(1) > :nth-child(4) > .form-control')
                .click()
                .type(Report.collectionChasedByData)
        }

        if (Report.CollectionNotes === true) {
            cy.get(':nth-child(12) > .border-div > .modal-body > :nth-child(2) > .row > .col-md-12 > .col-md-3 > .btn')
                .click()
        }
    });
})