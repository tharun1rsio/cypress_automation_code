/// <reference types="cypress" />
const loginData = require('../../fixtures/Login.json')
// const Placement = require('../../fixtures/Placement_module.json')
let rowsLength;

describe('Placement Creation', () => {
    before(() => {
        cy.task('readXlsx', { file: 'cypress/data/Placementmodule -RTesting.xlsx', sheet: "Placement_Creation" }).then((rows) => {
            rowsLength = rows.length;
            cy.writeFile("cypress/fixtures/Placement_module.json", { rows })
        })
    })
    it('Open the 1PS page', () => {
        cy.visit('/')
        Cypress.on('uncaught:exception', (err, runnable) => { return false; })
    })
    it('1PS login', () => {
        cy.Login(loginData.username, loginData.password)
        // cy.getCookie('Authorization').then((cookie) => {
        //     // cy.log(cookie.value);
        //     let accesstoken = cookie.value
        //     let apiBody = { "reference": "0977", "chasedId": "0977" }
        //     cy.apiRequest('https://1ps-dev-api.my1ps.com/contract/search', 'POST', accesstoken, apiBody)
        //         .then((response) => {
        //             let plcCount = response.body.content
        cy.fixture('Placement_module').then((data) => {
            for (let i = 0; i < rowsLength; i++) {
                let j = 1
                // for (let i = 1; i <= plcCount.length; i++) {
                // cy.get('a > .icon-container > .fa')
                //     .click()
                //     .wait(3000)
                // cy.log(JSON.stringify(plcCount.length))

                // Send to client
                // cy.get(':nth-child(2) > a > p')
                //     .click()
                // cy.get('div.icon-container > .fa')
                //     .click()
                // cy.get(':nth-child(1) > .form-group > .form-control')
                //     .type(data.rows[i].contractorRef)
                // cy.get('.btn-success')
                //     .click()
                //     .wait(2000)
                // cy.get(':nth-child(' + i + ') > :nth-child(8) > a > span')
                //     .click()
                //     .wait(3000)
                // cy.get('#Button')
                //     .click()
                // cy.log('Send to client button clicked')
                //     .wait(8000)
                // Approve button click
                cy.get(':nth-child(2) > a > p')
                    .click()
                cy.get('div.icon-container > .fa')
                    .click()
                cy.get(':nth-child(1) > .form-group > .form-control')
                    .type(data.rows[i].contractorRef)
                cy.get('.btn-success')
                    .click()
                    .wait(2000)
                cy.get(':nth-child(' + j + ') > :nth-child(8) > a > span')
                    .click()
                    .wait(3000)
                cy.get('#approveButton')
                    .click()
                cy.log('Approve button clicked')
                    .wait(8000)
                // Accept placement
                cy.get(':nth-child(2) > a > p')
                    .click()
                cy.get('div.icon-container > .fa')
                    .click()
                cy.get(':nth-child(1) > .form-group > .form-control')
                    .type(data.rows[i].contractorRef)
                cy.get('.btn-success')
                    .click()
                    .wait(2000)
                cy.get(':nth-child(' + j + ') > :nth-child(8) > a > span')
                    .click()
                    .wait(3000)
                cy.get('#acceptButton')
                    .click()
                cy.log('Accepted button clicked')
                    .wait(8000)
            }
        })
    })
})