/// <reference types="cypress" />
const loginData = require('../../fixtures/Login.json')
const Placement = require('../../fixtures/Placement_module.json')
const Expenses = require('../../fixtures/Expenses_module.json')

describe('Expenses Creation', () => {
    it('Open the 1PS page', () => {
        cy.visit('http://localhost:3000/')
        Cypress.on('uncaught:exception', (err, runnable) => { return false; })
    })
    it('1PS login', () => {
        cy.Login(loginData.username, loginData.password)
        cy.get(':nth-child(4) > a > p')
            .click()
    })
    it('Navigate to expenses creation screen', () => {
        cy.get('a > .icon-container > .fa')
            .click()
            .wait(3000)
    })
    it('Select expenses type', () => {
        cy.get('input[value="' + Expenses.expenseType + '"]')
            .check()
    });
    it('control for with placement or without placement', () => {
        if (Expenses.expenseType === 1) {// with placement 
            cy.get(':nth-child(2) > .form-group > :nth-child(2) > .input-group > #searchTextBox')
                .click()
                .type(Expenses.contractorName)
                .get('#responseListItem0')
                .click()
                cy.get('#searchTextBox')
                .click()
                .type(Expenses.placement)
                .get('#responseListItem0')
                .click()
            cy.get('#expenseType')
                .should('have.text', Expenses.expensesType)//change placement data
            cy.get('#serviceCharge')
                .should('have.text', Expenses.serviceCharge)//change placement data

        } else {// without placement
            cy.get(':nth-child(2) > .form-group > :nth-child(2) > .input-group > #searchTextBox')
                .click()
                .type(Expenses.contractorName)
                .get('#responseListItem0')
                .click()
            cy.get(':nth-child(3) > .form-group > :nth-child(2) > .input-group > #searchTextBox')
                .click()
                .type(Expenses.agencyName)
                .get('#responseListItem0')
                .click()
            cy.get(':nth-child(4) > .form-group > :nth-child(2) > .input-group > #searchTextBox')
                .click()
                .type(Expenses.clientComapny)
                .get('#responseListItem0')
                .click()
            cy.get('#expenseType')
                .select(Expenses.expensesPaymentType)
                .type('{enter}')
            cy.get('#serviceCharge')
                .should('have.text', Expenses.serviceCharge)//change placement data
            cy.get('#clientPaymentTerms')
                .select(Expenses.paymentTerms)
                .type('{enter}')
            cy.get('#currencyundefined > :nth-child(2) > .input-group > #searchTextBox')
                .click()
                .type(Expenses.currency)
                .get('#responseListItem0')
                .click()
            cy.get('#currencyPercentageundefined > .form-control')
                .select(Expenses.tax)
                .type('{enter}')
        }
    });
    it('Enter Desc, date and other details', () => {
        cy.get('.datepicker')
            .click()
            .type(Expenses.description)
        cy.get('.col-md-2.is-empty > .form-group > .form-control')
            .click()
            .type(Expenses.date)
        cy.get(':nth-child(3) > .form-group > .form-control')
            .click()
            .type(Expenses.amount)
        cy.get(':nth-child(8) > :nth-child(1) > .form-group > .form-control')
            .click()
            .type(Expenses.totalAmount)
        cy.get(':nth-child(8) > :nth-child(2) > .form-group > .form-control')
            .click()
            .type(Expenses.totalDate)
        cy.get('textarea')
            .click()
            .type(Expenses.comments)
    });
    it('Enter submit details', () => {
        cy.get('#saveButton')
            // .click()
        cy.end()
    });
})