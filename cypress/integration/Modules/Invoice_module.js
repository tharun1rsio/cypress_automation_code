/// <reference types="cypress" />
import dateformat from 'dateformat';
const DATEFORMAT = "dd-mmm-yyyy"
const loginData = require('../../fixtures/Login.json')
// const Placement = require('../../fixtures/Placement_module.json')
const Invoices = require('../../fixtures/Invoice_data.json')
// let countryCode;
// let currencyCode;
// let amount;
const invAmount = function (countryCode, currencyCode, amount) { new Intl.NumberFormat(countryCode, { style: 'currency', currency: currencyCode }).format(amount).replace(/\u00a0/g, ' ') }

describe('Invoice data validation', () => {
    it('Open the 1PS page', () => {
        cy.visit('http://localhost:3000/')
        Cypress.on('uncaught:exception', (err, runnable) => { return false; })
    })

    it('Invoice Validation', () => {
        cy.Login(loginData.username, loginData.password)
        cy.getCookie('Authorization').then((cookie) => {
            // cy.log(cookie.value);
            // let accesstoken = cookie.value
            // cy.request({
            //     url: 'https://1ps-dev-api.my1ps.com/invoice/INV-4925',
            //     method: 'GET',
            //     headers: {
            //         authorization: accesstoken
            //     },
            // })
            cy.apiRequest('https://1ps-dev-api.my1ps.com/invoice/INV-4925', 'GET', cookie.value)

        })
            .should((response) => {
                // cy.log(JSON.stringify(response.body))
                // cy.writeFile('cypress/fixtures/Invoice_data.json', response.body)
                cy.get(':nth-child(5) > a > p')
                    .click()
                cy.get('.card-header > :nth-child(1) > .fa')
                    .click()
                    .wait(3000)
                cy.get(':nth-child(12) > .input-group-mb3 > .form-control')
                    .click()
                    // .type(Invoices.tsRef)
                    .type(Invoices.timesheetRef)
                cy.get('.btn-success')
                    .click()
                    .wait(5000)
                cy.get(':nth-child(2) > [title="View"] > a > span')
                    .click()
                    .wait(2000)
                var dateInv = dateformat(new Date(response.body.invoice.createdOn), DATEFORMAT, true);
                cy.get('#invoice > :nth-child(2)')
                    .should('have.text', 'Date of invoice: ' + dateInv)
                cy.get('#company > .name')
                    .should('have.text', response.body.invoice.invoiceStatus[0].companyName)
                cy.get('#client > h2.name')
                    .should('have.text', response.body.invoice.toCompany.name)
                // .should(Currency, {
                //     quantity: response.body.invoice.invoiceAmount,
                //     currency: response.body.invoice.currencyTaxPercentage.currency.currencyCode
                // });
                // cy.log(Currency)
                cy.CurrencyCnv('de-DE', response.body.invoice.currencyTaxPercentage.currency.currencyCode, response.body.invoice.invoiceAmount)
                // cy.log(invAmount('de-DE', response.body.invoice.currencyTaxPercentage.currency.currencyCode, response.body.invoice.invoiceAmount))
                // var invAmount = function (countryCode) {
                //     new Intl.NumberFormat(countryCode, { style: 'currency', currency: response.body.invoice.currencyTaxPercentage.currency.currencyCode }).format(response.body.invoice.invoiceAmount)
                // }
                // switch (response.body.invoice.currencyTaxPercentage.currency.currencyCode) {

                //     case "EUR":
                //         var invAmount = new Intl.NumberFormat('de-DE', { style: 'currency', currency: response.body.invoice.currencyTaxPercentage.currency.currencyCode }).format(response.body.invoice.invoiceAmount).replace(/\u00a0/g, ' ')
                //         // let invEuroCurrency = response.body.invoice.invoiceAmount.toLocaleString('de-DE');
                //         // const number = response.body.invoice.invoiceAmount;

                //         // cy.log(new Intl.NumberFormat('de-DE', { style: 'currency', currency: response.body.invoice.currencyTaxPercentage.currency.currencyCode }).format(response.body.invoice.invoiceAmount));
                //         // var Invamount = new Intl.NumberFormat('de-DE', { style: 'currency', currency: response.body.invoice.currencyTaxPercentage.currency.currencyCode }).format(response.body.invoice.invoiceAmount)
                //         // var Invamount = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number).replace(/\u00a0/g, ' ')
                //         // var formatter = new Intl.NumberFormat('de-DE', {
                //         //     style: 'currency',
                //         //     currency: response.body.invoice.currencyTaxPercentage.currency.currencyCode,
                //         // });
                //         // var Invamount = formatter.format(response.body.invoice.invoiceAmount)
                //         // cy.log(Invamount)
                //         cy.get('tfoot > :nth-child(1) > :nth-child(2)')
                //             .should('have.text', invAmount)
                //         // cy.get('tfoot > :nth-child(1) > :nth-child(2)')
                //         //     .should('have.text', invEuroCurrency + ' ' + response.body.invoice.currencyTaxPercentage.currency.currencySymbol)
                //         break;
                //     case "INR":
                //         cy.get('tfoot > :nth-child(1) > :nth-child(2)')
                //             .should('have.text', response.body.invoice.invoiceAmount + ' ' + response.body.invoice.currencyTaxPercentage.currency.currencySymbol)
                //         break;
                //     case "GBP":
                //         // const number = response.body.invoice.invoiceAmount;
                //         // cy.log(new Intl.NumberFormat('en-GB', { style: 'currency', currency: 'GBP' }).format(number));
                //         // var Invamount = new Intl.NumberFormat('en-GB', { style: 'currency', currency: 'GBP' }).format(number)
                //         // cy.get('tfoot > :nth-child(1) > :nth-child(2)')
                //         //     .should('have.text', Invamount)
                //         cy.get('tfoot > :nth-child(1) > :nth-child(2)')
                //             .should('have.text', response.body.invoice.invoiceAmount + ' ' + response.body.invoice.currencyTaxPercentage.currency.currencySymbol)
                //         break;
                //     case "USD":
                //         cy.get('tfoot > :nth-child(1) > :nth-child(2)')
                //             .should('have.text', response.body.invoice.invoiceAmount + ' ' + response.body.invoice.currencyTaxPercentage.currency.currencySymbol)
                //         break;
                // }

                // cy.get('tfoot > :nth-child(2) > :nth-child(2)')
                //     .contains(response.body.invoice.vatAmount)
            })

    })
    // it('Validate 1PS invoices', () => {
    //     cy.get('.col-md-8 > .col-md-3 > div')
    //         .should('have.text', Invoices.invoice.invoiceStatus.status)
    // });
})