/// <reference types="cypress" />
import dateformat from 'dateformat';
const DATEFORMAT = "dd-mmm-yyyy"
const loginData = require('../fixtures/Login.json')
// const Placement = require('../../fixtures/Placement_module.json')
const Invoices = require('../fixtures/Invoice_data.json')
// const tsdata = require('../../fixtures/Invoice.json')

describe('Invoice data validation', () => {
    it('Open the 1PS page', () => {
        cy.visit('/')
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false;
        })
    })

    function Currency(currencyCode) {

        formatter = new Intl.NumberFormat(currencyCode, {
            style: 'currency',
            currency: response.body.invoice.currencyTaxPercentageCurrencyCode,
        });
        formatter.format(response.body.invoice.invoiceAmount)
        formatter.format(response.body.invoice.vatAmount)
        formatter.format(response.body.invoice.totalAmount)
    }

    it('Invoice Validation', () => {
        cy.Login(loginData.username, loginData.password)
        cy.getCookie('Authorization').then((cookie) => {
            // cy.log(cookie.value);
            let accesstoken = cookie.value
            // cy.request({
            //     url: 'https://1ps-dev-api.my1ps.com/invoice/search',
            //     method: 'POST',
            //     headers: {
            //         authorization: accesstoken
            //     },
            var apiBody = { "reference": "", "contractorCompanyName": "", "clientCompanyName": "", "agencyCompanyName": "", "position": "", "pageNumber": 1, "pageSize": 10, "fromDate": "", "toDate": "", "generatedDate": null, "fromCompanyName": "", "toCompanyName": "", "generatedFor": "", "dueDate": null, "submissionDate": null, "contractReference": "", "claimDate": null, "isManualInvoiceView": null, "amount": "", "invoiceId": "", "timeSheetId": Invoices.timesheetRef, "isSent": null, "communicationType": null, "communicationSub": null, "chasedBy": "", "chasedFor": "", "chasedId": null, "approvalDate": "", "approverName": "", "timesheetFrequency": "", "rateType": "", "purchaseOrderNumber": "", "approvalStartDate": "", "approvalEndDate": "", "submittedStartDate": "", "submittedEndDate": "", "paidStartDate": "", "paidEndDate": "", "placementRef": "", "expensesId": "", "startDate": "", "endDate": "", "createdAt": "", "createdBy": "", "contractorUserName": "" }
            cy.apiRequest('https://1ps-dev-api.my1ps.com/invoice/search', 'POST', accesstoken, apiBody)

                // })
                .then((response) => {
                    // for (let i = 0; i < Invoices.length; i++) {
                    //     var j = i + 1
                        cy.log(response)
                        // .pause()
                    let tsdata = response.body.content
                    cy.log(tsdata.length)
                    for (let i = 0; i < tsdata.length; i++) {
                        let j = i + 1;
                        // let inc =j
                        cy.log(j)
                        // cy.request({
                        //     url: 'https://1ps-dev-api.my1ps.com/invoice/' + tsdata[i].id,
                        //     method: 'GET',
                        //     headers: {
                        //         authorization: accesstoken
                        //     },

                        // })
                        cy.apiRequest('https://1ps-dev-api.my1ps.com/invoice/' + tsdata[i].id, 'GET', accesstoken)
                            .should((response) => {

                                cy.get(':nth-child(5) > a > p')
                                    .click()
                                    .wait(10000)

                                cy.get('.card-header > :nth-child(1) > .fa')
                                    .click()
                                    // .pause()
                                    .wait(5000)
                                cy.get(':nth-child(12) > .input-group-mb3 > .form-control')
                                    .click()
                                    .wait(5000)
                                    // .type(Invoices.tsRef)
                                    .type(Invoices.timesheetRef)

                                // eslint-disable-next-line cypress/no-unnecessary-waiting
                                cy.get('.btn-success')
                                    .click()
                                    .wait(5000)
                                // eslint-disable-next-line cypress/no-unnecessary-waiting
                                cy.get(':nth-child( ' + j + ') > [title="View"] > a > span')
                                    .click()
                                    .wait(3000)
                                cy.get('#company > .name')
                                    .should('have.text', response.body.invoice.fromCompany.name)
                                var dateInv = dateformat(new Date(response.body.invoice.createdOn), DATEFORMAT, true);
                                cy.get('#invoice > :nth-child(2)')
                                    .should('have.text', 'Date of invoice: ' + dateInv)
                                cy.get('#invoice > :nth-child(3)')
                                    .should('have.text', 'VAT registration number: ' + response.body.invoice.fromCompany.vatRegistrationNumber)
                                cy.get('#client > h2.name')
                                    .should('have.text', response.body.invoice.toCompany.name)
                                var dateInv = dateformat(new Date(response.body.invoice.payByDate), DATEFORMAT, true);
                                cy.get(':nth-child(4) > :nth-child(1) > b')
                                    .should('have.text', 'Due Date: ' + dateInv)

                                switch (response.body.invoice.currencyTaxPercentage.currency.currencyCode) {
                                    case "USD":

                                        var Invamount = response.body.invoice.invoiceAmount;
                                        var Inv = (Invamount.toLocaleString('en-us'))
                                        var Vat = (response.body.invoice.vatAmount.toLocaleString('en-US'))
                                        var grandTotal = (response.body.invoice.totalAmount.toLocaleString('en-US'))
                                        // var Currency={Invamount}, currency={currencyTaxPercentageCurrencySymbol}
                                        Currency('en-US')

                                        cy.get('tfoot > :nth-child(2) > :nth-child(2)')
                                            .should('have.text', formatter.format(response.body.invoice.vatAmount))

                                        cy.get('tfoot > :nth-child(1) > :nth-child(2)')
                                            .should('have.text', formatter.format(response.body.invoice.invoiceAmount))

                                        cy.get('tfoot > :nth-child(3) > :nth-child(2)')
                                            .should('have.text', formatter.format(response.body.invoice.totalAmount))

                                        break;
                                    case "EUR":
                                        var invAmount = new Intl.NumberFormat('de-DE', { style: 'currency', currency: response.body.invoice.currencyTaxPercentage.currency.currencyCode }).format(response.body.invoice.invoiceAmount).replace(/\u00a0/g, ' ')
                                        var vatAmount = new Intl.NumberFormat('de-DE', { style: 'currency', currency: response.body.invoice.currencyTaxPercentage.currency.currencyCode }).format(response.body.invoice.vatAmount).replace(/\u00a0/g, ' ')
                                        var totalAmount = new Intl.NumberFormat('de-DE', { style: 'currency', currency: response.body.invoice.currencyTaxPercentage.currency.currencyCode }).format(response.body.invoice.totalAmount).replace(/\u00a0/g, ' ')
                                        switch (j) {
                                            case 4:
                                                cy.get('tfoot > :nth-child(1) > :nth-child(3)')
                                                    .should('have.text', invAmount)
                                                cy.get('tfoot > :nth-child(2) > :nth-child(3)')
                                                    .should('have.text', vatAmount)
                                                cy.get('tfoot > :nth-child(3) > :nth-child(3)')
                                                    .should('have.text', totalAmount)
                                                break;
                                            default:
                                                cy.get('tfoot > :nth-child(1) > :nth-child(2)')
                                                    .should('have.text', invAmount)
                                                cy.get('tfoot > :nth-child(2) > :nth-child(2)')
                                                    .should('have.text', vatAmount)
                                                cy.get('tfoot > :nth-child(3) > :nth-child(2)')
                                                    .should('have.text', totalAmount)
                                        }
                                        break;
                                    case "INR":
                                        cy.get('tfoot > :nth-child(1) > :nth-child(2)')
                                            .should('have.text', Inv + ' ' + response.body.invoice.currencyTaxPercentage.currency.currencySymbol)

                                        cy.get('tfoot > :nth-child(3) > :nth-child(2)')
                                            .should('have.text', grandTotal + ' ' + response.body.invoice.currencyTaxPercentage.currency.currencySymbol)
                                        break;
                                    case "GBP":
                                        var Invamount = response.body.invoice.invoiceAmount;
                                        var Inv = (Invamount.toLocaleString('en-GB'))
                                        var Vat = (response.body.invoice.vatAmount.toLocaleString('en-GB'))
                                        var grandTotal = (response.body.invoice.totalAmount.toLocaleString('en-GB'))
                                        if (response.body.invoice.vatAmount === 0) {
                                            cy.get('tfoot > :nth-child(2) > :nth-child(2)')
                                                .should('have.text', '0,00' + ' ' + response.body.invoice.currencyTaxPercentage.currency.currencySymbol)
                                        } else {
                                            cy.get('tfoot > :nth-child(2) > :nth-child(2)')
                                                .should('have.text', Vat + ' ' + response.body.invoice.currencyTaxPercentage.currency.currencySymbol)
                                        }
                                        cy.get('tfoot > :nth-child(3) > :nth-child(2)')
                                            .should('have.text', grandTotal + ' ' + response.body.invoice.currencyTaxPercentage.currency.currencySymbol)
                                        cy.get('tfoot > :nth-child(1) > :nth-child(2)')
                                            .should('have.text', Inv + ' ' + response.body.invoice.currencyTaxPercentage.currency.currencySymbol)
                                }
                            })
                    }
                })
        })
    })
})